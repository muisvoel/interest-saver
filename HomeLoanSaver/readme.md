# Home Loan Saver
A timer trigger Azure function which helps to minimise the interest paid on a home loan. This was developed on 
the assumption that all values being checked are in ZAR (can be changed), you have an active Home Loan and a regular monthly income deposited into your Transactional Account. This regular deposit was assumed to be a salary for proof-of-concept, but this can be extended to any arbitrary large amount that enters your account, such as bonuses, rental income, investment payout etc. 

The benefit of this is only evident in the long run by saving a lot on interest payment, since your outstanding capital is less for short periods of time during the month. In effect, you can save hundreds of thousands of Rand on interest payments. 